#trb sa verific daca cel de-al doilea element este un operator aritmetic

if ! (test "$#" -eq 3)
then
    echo "nu am primit nr corect de argumente"
    exit 1
fi
#in if-ul asta am 1 problema
#de ce daca pun operatorul * nu mi-l recunoaste
if ! (test "$2" = '+' || test "$2" = '-' || test "$2" = '/' || test "$2" = 'x')
then
    echo "nu am primit un operator aritmetic valid"
    exit 1
fi
echo "totul bine pana acum"

if test "$2" = '+'
then
    var=`expr $1 + $3`
    echo "$var"
elif test "$2" = '-'
then
     var=`expr $1 - $3`
     echo "$var"
elif test "$2" = '\'
then
     var=`expr $1 \ $3`
     echo "$var"
elif test "$2" = 'x'
then
     var=`expr $1 * $2`
     echo "$var"
fi
: '
nu inteleg de ce nu imi face pt cazurile 3 si4
si care e diferenta intre `expr $1+$2` de ce imi afiseaza 1+2
in loc de rezultatul care ar trebui
'

#varianta cu case

