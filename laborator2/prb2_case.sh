: '
Să se scrie un shell/bash script care primește 3 argumente pe linie de comandă: primul și al treilea argument vor reprezenta operanzii, iar al doilea va reprezenta un operator aritmetic.

Să se efectueze operația primită și să se afișeze rezultatul.

Script-ul se va apela sub forma:


./script.sh <operand1> <operator> <operand2>
'
if ! (test "$#" -eq 3)
then
    echo "nu am primit nr corect de argumente"
    exit 1
fi
case "$2" in
    "+") var=`expr $1 + $3`;echo "$var";;
    "-") var=`expr $1 - $3`;echo "$var";;
    '/') var=`expr $1 / $3`;echo "$var";;
    '*')var=`expr $1 *  $3`;echo "$var";;
    *) echo "nu s a primit un operator aritmetic valid";;
esac
