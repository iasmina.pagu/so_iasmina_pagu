date2=$(date +%H)
cale=$(whoami)
case $date2 in
    0[6-9]|1[0-1]) echo "Buna dimineata, $cale";;
    1[2-7]) echo "Buna ziua, $cale";;
    1[7-9]|2[0-1]) echo "Buna seara, $cale";;
    2[2-3]|00) echo "Noapte buna, $cale";;
    *) echo "Eroare";;
esac

